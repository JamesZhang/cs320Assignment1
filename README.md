James Zhang
zhangjames389@gmail.com

The first hw assigment for Cs320 Fall 2016. This assigment is intended to help the user get used to programming in C and command line arguments.

prog1_1.c: Basic printing program that introduces the programmer.

prog1_2.c: Practice using std input and output. Prompts user for name and then greets them.

prog1_3.c: Calculates the the hyperbolic cosine of a number in degrees. Must be compiled with -lm to link the math library when building the executable. i.e "gcc prog1_3.c -lm"

prog1_4.c: Produces a a cosine wave from command line arguments consisting of number of elements and step size in degrees. Prints out x and y axis values(up to two decimal places) as two parallel arrays

