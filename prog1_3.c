#include <stdio.h>
#include <math.h>

int degree;
double x;
double pi = 3.141592;

int main()
{
	printf("Assignment #1-3, James Zhang, zhangjames389@gmail.com \n");
	printf("Please input an integer: \n");
	scanf("%d", &degree);
	x = degree * pi/180;		/* converts degrees to radians */
	printf("%.3lf \n", cosh(x));

	return 0;

}
