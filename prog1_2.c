#include <stdio.h>

char name[100];

int main()
{
	printf("Assignment #1_2, James Zhang, zhangjames389@gmail.com \n");
	printf("Hello! What is your name? \n");
	scanf("%s", name);
	printf("Hello %s. Nice to meet you! \n", name);

	return 0;
}
